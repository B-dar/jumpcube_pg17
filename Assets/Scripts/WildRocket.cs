﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WildRocket : Projectile
{
    [Header("Swarming")]
    [SerializeField] private float _rotationSpeed = 10f;    // how fast our rocket can turn
    [SerializeField] private float _derpiness = 15f;        // how inaccurate our rocket will be
    [SerializeField] private float _perlinSpeed = 1f;       // how quickly rocket changes direction

    [Header("Seeking")]
    [SerializeField] private float _searchDistance = 8f;    // how far to look for targets
    [SerializeField] private LayerMask _searchMask;         // what layers to look for targets on

    private float _perlinOffset;
    private Health _target;

    protected override void Start()
    {
        // call base start
        base.Start();

        // random value between 0f - 1f
        _perlinOffset = Random.value;
    }

    private void FixedUpdate()
    {
        // check if target exists
        if(_target == null)
        {
            // find all Health components in range
            Collider2D[] characterColliders = Physics2D.OverlapCircleAll(transform.position, _searchDistance, _searchMask);
            // find valid enemy in collider array
            for (int i = 0; i < characterColliders.Length; i++)
            {
                // get potential health components
                Health health = characterColliders[i].GetComponent<Health>();
                // filter for non-null, alive, and on enemy team
                if(health != null && health.IsAlive && _team != health.Team)
                {
                    _target = health;
                }
            }
        }
        else
        {
            // direction to target
            Vector3 dirToTarget = (_target.Position - transform.position).normalized;
            Vector3 forward = Vector3.forward;
            // combining quaternion rotation and direction together
            Vector3 up = Quaternion.Euler(0f, 0f, 90f) * dirToTarget;
            // get desired rotation
            Quaternion targetRotation = Quaternion.LookRotation(forward, up);
            // get interpolated rotation
            Quaternion rotation = Quaternion.Lerp(transform.rotation, targetRotation, _rotationSpeed * Time.fixedDeltaTime);
            // set our rotation
            transform.rotation = rotation;
        }

        // get perlin value
        float perlinValue = Mathf.PerlinNoise(Time.timeSinceLevelLoad * _perlinSpeed, _perlinOffset);
        float remappedPerlin = perlinValue * 2f - 1f;
        float adjustmentAngle = remappedPerlin * _derpiness;    // angle adjustment

        // create Quaterion from Euler angle
        Quaternion adjustmentRotation = Quaternion.Euler(0f, 0f, adjustmentAngle);

        // add adjustment rotation to current rotation (using multiplication)
        transform.rotation *= adjustmentRotation;

        // move "forward"
        _rigidbody.velocity = transform.right * _velocity;
    }
}