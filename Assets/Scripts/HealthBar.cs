﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    // the health component we're displaying
    [SerializeField] private Health _target;

    private Image _image;

    private void Start()
    {
        _image = GetComponent<Image>();
    }

    private void Update()
    {
        // set to 0 if target is null
        if(_target == null)
        {
            _image.fillAmount = 0f;
            return;
        }

        // set fill to health percentage
        _image.fillAmount = _target.HealthPercentage;
    }
}