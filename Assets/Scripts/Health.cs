﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    [SerializeField] private float _current = 100f;
    [SerializeField] private float _max = 100f;
    [SerializeField] private int _team = 1;

    public bool IsAlive => _current > 0f;
    public int Team => _team;
    public float HealthPercentage => _current / _max;
    public Vector3 Position => transform.position + new Vector3(0f, 0.75f, 0f);

    // unity event
    public UnityEvent DeathEvent;

    public void Damage(float amount)
    {
        // return early if dead
        if(!IsAlive) return;

        // deal damage
        _current -= amount;

        // call death event
        if(!IsAlive) DeathEvent?.Invoke();
    }
}
